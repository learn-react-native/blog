import React, { useContext } from "react";
import { StyleSheet } from "react-native";
import { Context } from "../context/BlogContext";
import BlogPostForm from "../components/BlogPostForm";

const EditScreen = ({ route, navigation }) => {
  const { id } = route.params;
  const { state, editBlogPost } = useContext(Context);
  const blogPost = state.find((blogPost) => blogPost.id === id);

  return (
    <BlogPostForm
      initialValues={{ title: blogPost.title, content: blogPost.content }}
      onSubmit={(title, content) => {
        // console.log(title, content);
        editBlogPost(id, title, content, () => navigation.pop());
      }}
    ></BlogPostForm>
  );
  //   return (
  //     <View style={styles.container}>
  //       <Text style={styles.title}>Enter New Title: </Text>
  //       <TextInput
  //         style={styles.textInput}
  //         value={title}
  //         onChangeText={(newText) => {
  //           setTitle(newText);
  //           console.log(newText);
  //         }}
  //       ></TextInput>
  //       <Text style={styles.title}>Enter New Content:</Text>
  //       <TextInput
  //         style={styles.textInput}
  //         value={content}
  //         onChangeText={(newText) => setContent(newText)}
  //       ></TextInput>
  //       <Button
  //         title="Save"
  //         onPress={() => {
  //           addBlogPost(title, content, () => {
  //             navigation.navigate("Index");
  //           });
  //         }}
  //       ></Button>
  //     </View>
  //   );
};
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
  },
  title: {
    fontSize: 20,
    paddingBottom: 20,
  },
  textInput: {
    paddingBottom: 10,
    borderColor: "gray",
    borderWidth: 1,
    fontSize: 18,
  },
});
export default EditScreen;

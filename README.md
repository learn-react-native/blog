# Blog App React Native

Build app mobile with React Native with ngrok serverjson:
- Add Blog (title, content)
- Edit Blog (title, content)
- Delete Blog

## Getting Started

First, you should clone the server code, and don't forget to download ngrok at https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-windows-amd64.zip
 - clone server ngrok in branch jsonserver
Second
```
npm install json-server ngrok
```
To run server
```
npm run db
```
### Installing

Continue run app Blog 


Git clone code in branch feature/lesson13


Later

```
npm install
```

Remember to install expo app at App store or CHPlay

## Running the tests

```
expo start
or
npm start
or 
yarn start
```
### The images in the application

Home Screen to view all Blog

![alt text](https://gitlab.com/learn-react-native/blog/-/raw/master/assets/117687832_1166386930398858_6650331062066231447_n.jpg?raw=true)

OnPress Button plus at left root to Create Blog

![alt text](https://gitlab.com/learn-react-native/blog/-/raw/master/assets/118127947_321099345981348_2991499269275870454_n.jpg?raw=true)

enter title and content of blog and on press save

![alt text](https://gitlab.com/learn-react-native/blog/-/raw/master/assets/118339924_663187374404790_5107484991102804235_n.jpg?raw=true)

OnPress one blog in screen to show detail blog

![alt text](https://gitlab.com/learn-react-native/blog/-/raw/master/assets/118170200_235504910984887_6293665107001515803_n.jpg?raw=true)

OnPress Button edit at left root to Edit blog

![alt text](https://gitlab.com/learn-react-native/blog/-/raw/master/assets/117838588_695605987835516_9148372362749396613_n.jpg?raw=true)


## Contact me

* [Nguyen Vo Trai](https://www.facebook.com/nguyen.votrai/) - FaceBook of me
* Gmail - trainguyen1199@gmail.com
